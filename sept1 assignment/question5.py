import os
import shutil

def creation_of_files():
    a = 0
    while a < 100:
        a += 1
        path = r'C:\Users\nsurisetti\Desktop\testingfiles'
        file_name = str(a)+'.txt'
        with open(os.path.join(path, file_name), 'w') as fp:
            fp.write('This is a test file')

def copy_rename_files():
    src = 'C:\\Users\\nsurisetti\\Desktop\\testingfiles'
    des = 'C:\\Users\\nsurisetti\\Desktop\\destination_testing'
    prefix = input("enter prefix : ")
    suffix = int(input("start numbering from : "))

    for file_name in os.listdir(src):
        src_file = os.path.join(src,file_name)
        des_file = os.path.join(des,prefix+str(suffix)+".txt")
        suffix += 1
        shutil.copy(src_file,des_file)

creation_of_files()
copy_rename_files()
