'''
Python program for Random Password Generating and Strongness Checking.
'''

import random,string

class password:
	'''
	class to generate random password and check strongness of the password
	...

	Methods
	-------
	random_password(self,length,specialChar)
		generates the random password with given parameters

	pass_strength(self,*args)
		checks the strength of the given password
  
	'''
	def random_password(self,length,specialChar):
		'''
		method to generate the random password with given parameters
		...

		Parameters
		----------
		length : Integer
			length of the password you want to generate

		specialChar: Integer
			number of special characters you want in your password
		'''
		## characters to generate password from
		special_char = list("!@#$%^&*()")
		characters = list(string.ascii_letters + string.digits)

		# print not valid if the special_characters_count is greater than length of password
		if specialChar > length:
			print("Characters total count is greater than the password length")
			return

		# initializing the empty list for storing password
		password = []
		
		# picking special characters
		for i in range(specialChar):
			password.append(random.choice(special_char))

		# add random characters to make it equal to the length
		if specialChar < length:
			random.shuffle(characters)
			for i in range(length - specialChar):
				password.append(random.choice(characters))

		# shuffling the resultant password
		random.shuffle(password)

		## converting the list to string
		## printing the list
		print("New Password : "+"".join(password))

	def pass_strength(self,*args):
		'''
		method to check strength of the given password
		
		...
		
		Parameters
		----------
		args : string
			password that you want to check for its strongness

		'''
		# loop through the arguments 
		for x in args:
			n = len(x)

			# Checking lower alphabet in string
			hasLower = False
			hasUpper = False
			hasDigit = False
			specialChar = False
			normalChars = "abcdefghijklmnopqrstu"
			"vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "
			
			for i in range(n):
				if x[i].islower():
					hasLower = True
				if x[i].isupper():
					hasUpper = True
				if x[i].isdigit():
					hasDigit = True
				if x[i] not in normalChars:
					specialChar = True

			# Strength of password
			print("Strength of "+x+" is : ", end = "")
			
			# if all the below conditions are satisfied then it prints strong
			if (hasLower and hasUpper and
				hasDigit and specialChar and n >= 8):
				print("Strong")
			
			# if the below conditions are satisfied then it prints moderate
			elif ((hasLower or hasUpper) and
				specialChar and n >= 6):
				print("Moderate")
			
			# if all the above conditions are failed the it prints weak
			else:
				print("Weak")

if __name__=="__main__":
	x = password()
	x.random_password(length=10,specialChar=2)
	x.pass_strength("abc3","Abc123","Nav@1905")
