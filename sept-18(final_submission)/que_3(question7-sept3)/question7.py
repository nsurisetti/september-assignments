"""
Object oriented program to merge two linked lists alternatively.
"""


class Node:
    """
    Creates a node for the elements in the lists
    ...
    Methods
    -------
    __init__(self, data, next)
            Initialize an instance with attribute data as data and next as
            next
    """

    def __init__(self, data=None, node=None):
        self.data = data
        self.next = node

    @staticmethod
    def printing_linked_list(message, head):
        """
        function to print the given linkedlist
        ...
        Parameters
        ----------
        message : string

        Returns
        -------
        string
                        Null if list is empty
        List
                        The content of linked list in the form of the list
        """
        print(message, end="")
        ptr = head
        while ptr:
            print(ptr.data, end=" —> ")
            ptr = ptr.next
        print("null")

    @staticmethod
    def merge_alternatively(a_1, b_1):
        """
        Recursive function to construct a linked list by merging
        alternate nodes of two given linked lists
        ...
        Parameters
        ----------
        a : list
        b : list

        Returns
        -------
        list
                        The content of linked list in the form of the list
        """

        # to see if either list is empty
        if a_1 is None:
            return b_1

        if b_1 is None:
            return a_1

        # it turns out to be convenient to do the recursive call first;
        # otherwise, `a.next` and `b.next` need temporary storage
        recur = Node.merge_alternatively(a_1.next, b_1.next)
        result = a_1  # one node from a
        a_1.next = b_1  # one from b
        b_1.next = recur  # then rest of the elements
        return result


# Driver program
if __name__ == "__main__":
    a = b = None
    arr1 = list(map(int, input().split()))
    arr2 = list(map(int, input().split()))

    for i in reversed(arr1):
        a = Node(i, a)

    for i in reversed(arr2):
        b = Node(i, b)

    # printing both lists
    Node.printing_linked_list("First List: ", a)
    Node.printing_linked_list("Second List: ", b)

    # calling the function
    res = Node.merge_alternatively(a, b)
    Node.printing_linked_list(
        "\nFinal list after merging the two lists alternatively : ", res
    )
