import pytest
from question3 import linkedlist_operations

list_empty_error = "LinkedList is empty, doesn't have elements to remove"
empty="list is empty"

def test_linkedlist():
    l1=linkedlist_operations()
    assert(list_empty_error==l1.delete_at_beginning())

    l1.insert_at_beginning(10)
    l1.insert_at_beginning("hello")
    l1.insert_at_beginning(None)
    l1.insert_at_beginning(1.2)
    assert([1.2,None,"hello",10]==l1.printlist())

    l1.delete_at_beginning()
    l1.delete_at_beginning()
    assert(["hello",10]==l1.printlist())

    l1.insert_at_beginning({1,2,3,4})
    assert([{1,2,3,4},"hello",10]==l1.printlist())
    
    l1.insert_at_beginning([1,2,{"username":"Nan"},"Test"])
    assert([[1,2,{"username":"Nan"},"Test"],{1,2,3,4},'hello',10]==l1.printlist())

    l1.delete_at_beginning()
    assert([{1,2,3,4},'hello',10]==l1.printlist())

    l1.delete_at_beginning()
    assert(['hello',10]==l1.printlist())

    l1.delete_at_beginning()
    l1.delete_at_beginning()
    assert(list_empty_error==l1.delete_at_beginning())
    

