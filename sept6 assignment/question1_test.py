import unittest
import question1
class TestCal(unittest.TestCase):
    def test_add(self):
        self.assertEqual(question1.add(1,2),3)
    
    def test_add_notequal(self):
        self.assertNotEqual(question1.add(1,2),4)

    def test_add_True(self):
        self.assertTrue(question1.add(1,2)==3,"the result is false")

    def test_add_False(self):
        self.assertFalse(question1.add(1,2)==4,"the result is true")

    def test_add_Is(self):
        self.assertIs(question1.add(1,2),question1.add(1,2),"does not match")
    
    def test_add_IsNot(self):
        self.assertIsNot(question1.add(1,2),question1.add(1,3),"match")

    # def test_add_IsNone(self):
    #     self.assertIsNone(question1.add(1,2),not none")

    def test_add_IsNotNone(self):
        self.assertIsNotNone(question1.add(1,2),"none")
    
    def test_add_In(self):
        self.assertIn(question1.add(1,2),[3,26,27],"not in list")

    def test_add_almostEqual(self):
        self.assertAlmostEqual(question1.add(1,2),3.000000001)

    def test_add_almostnotEqual(self):
        self.assertNotAlmostEqual(question1.add(1,2),5.0985001)

    def test_add_greater(self):
        self.assertGreater(question1.add(1,2),1)

if __name__=='__main__':
    unittest(question1())
