class CreateNode:
	'''
	Creats a node for the elements in the lists

	...

	Methods
	-------
	__init__(self, value)
		Initialize an instance with attribute value as value and next as 
			None
	
	__str__(self)
		returns value in the attribute value
	'''
	def __init__(self, value):
		self.value = value
		# the pointer initially points to nothing
		self.next = None

	def __str__(self):
		'''
		prints value in the Node
		'''
		return(f'{self.value}')

class linkedlist_operations:
    def __init__(self):
		
        '''
		Contructed setting head as None when object is create
		'''
        self.head = None
        self._length = 0

    def prepend(self, value):
            '''
            adds element at the begining of the linked list
            ...
            Parameters
            ----------
            value : Any
                value that should be prepended to the linked list

            '''
            # replacing head with value and setting it has head
            newNode = CreateNode(value)
            newNode.next = self.head
            self.head = newNode
            self._length = self._length + 1

    def removefirstelement(self):
            '''
            removes the first element at the stating of the linked list

            Raises
            ------
            ValueError
                when delete is called on empty linked list
            '''
            # when linked list is empty
            if self.head is None:
                return ("LinkedList is empty, doesn't have elements to remove")
            
            # adding the starting element
            pointer = self.head.next 
            self.head.next = None
            self.head = pointer
            self._length = self._length - 1

    def printlist(self):
            ''' 
            prints the currently stored linked list 
            
            Raises
            ------
            ValueError 
                when linked list is empty
            
            '''
            if self.head is None:
                print("Empty LinkedList")
            else:
                pointer = self.head 
                new = []
                while(pointer):
                    new.append(pointer.value)
                    pointer=pointer.next 
                return new
            

if __name__ == "__main__":
    linkedList = linkedlist_operations()
    linkedList.prepend(10)
    linkedList.prepend(20)
    # linkedList.removefirstelement()
    print(linkedList.printlist())