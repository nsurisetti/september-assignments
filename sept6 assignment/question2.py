def ispalindrome(inp):
    if type(inp) != str and type(inp) != list:
        return False
    if type(inp)==str:
        inp=inp.lower()
    res=inp[::-1]
    if inp == res:
        return True
    return False

if __name__=='__main__':
    print(ispalindrome("mom"))
    print(ispalindrome([1,2,1]))
    print(ispalindrome("daddy"))