'''
	Python program to create a two dimensional array in numpy and
	rotate it by 90 degrees clockwise
'''

# import the necessary modules required
import numpy as np

# creating and shaping the 2D array
new_arr = np.arange(9).reshape(3, 3)

N = 3


def rotate_clockwise(arr):
    """
    function to rotate the array by 90 degrees clockwise
    ...

    Parameters
    ----------
    arr : list of integers

    Returns
    -------
    list
                    elements of the list
    """
    # global N

    # printing the matrix on the basis of observations made on indices.
    for j in range(N):
        for i in range(N - 1, -1, -1):
            print(arr[i][j], end=" ")
        print()


print(new_arr)

# function call
rotate_clockwise(new_arr)
