"""
Object oriented program to merge two linked lists alternatively.
"""


class Node:
    """
    Creates a node for the elements in the lists
    ...
    Methods
    -------
    __init__(self, data, next)
            Initialize an instance with attribute data as data and next as
            next
    """

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    @staticmethod
    def printing_linked_list(message, head):
        """
        function to print the given linkedlist
        ...
        Parameters
        ----------
        message : string

        Returns
        -------
        string
                        Null if list is empty
        List
                        The content of linked list in the form of the list
        """
        print(message, end="")
        ptr = head
        while ptr:
            print(ptr.data, end=" —> ")
            ptr = ptr.next
        print("null")

    @staticmethod
    def merge_alternatively(a, b):
        """
        Recursive function to construct a linked list by merging
        alternate nodes of two given linked lists
        ...
        Parameters
        ----------
        a : list
        b : list

        Returns
        -------
        list
                        The content of linked list in the form of the list
        """

        # to see if either list is empty
        if a is None:
            return b

        if b is None:
            return a

        # it turns out to be convenient to do the recursive call first;
        # otherwise, `a.next` and `b.next` need temporary storage
        recur = Node.merge_alternatively(a.next, b.next)
        result = a  # one node from a
        a.next = b  # one from b
        b.next = recur  # then rest of the elements
        return result


# Driver program
if __name__ == "__main__":
    a = b = None
    arr1 = list(map(int, input().split()))
    arr2 = list(map(int, input().split()))

    for i in reversed(arr1):
        a = Node(i, a)

    for i in reversed(arr2):
        b = Node(i, b)

    # printing both lists
    Node.printing_linked_list("First List: ", a)
    Node.printing_linked_list("Second List: ", b)

    # calling the function
    head = Node.merge_alternatively(a, b)
    Node.printing_linked_list(
        "\nFinal list after merging the two lists alternatively : ", head
    )
