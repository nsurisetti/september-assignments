import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
import warnings 
warnings.filterwarnings('ignore')

df = pd.read_csv("breastCancerDataWisconsin.csv")

from sklearn.model_selection import train_test_split as tts

X = df.drop("diagnosis",axis = 1)
X.shape

Y = df["diagnosis"]

X_train,X_test,Y_train,Y_test = tts(X,Y,test_size = 0.2,train_size = 0.8)

X_train.shape

from sklearn import svm

clf = svm.SVC()

clf.fit(X_train, Y_train)

y_pred=clf.predict(X_test)

y_pred

from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

print(accuracy_score(Y_test, y_pred)*100)
print(confusion_matrix(Y_test, y_pred))
print(classification_report(Y_test,y_pred))
