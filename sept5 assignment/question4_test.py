import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
import warnings
warnings.filterwarnings('ignore')
from numpy import genfromtxt
df = genfromtxt('AdmissionPredict.csv', delimiter=',')

from sklearn.model_selection import train_test_split as tts

X = df[1:,1:-1]
y = df[1:,-1]

from sklearn.linear_model import LinearRegression
from sklearn import metrics
x_train, x_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=0)
Lin_reg = LinearRegression()
Lin_reg.fit(x_train, y_train)
print(Lin_reg.intercept_)
print(Lin_reg.coef_)
print(Lin_reg.score(x_test, y_test))

y_pred = Lin_reg.predict(x_test)

print(y_pred)

print(y_test)

width=0.5
plt.rcParams["figure.figsize"] = [30, 20]
bar1=np.arange(len(y_test))
bar2=[i+width for i in bar1]
plt.bar(bar1,y_test,width,label="y_test")
plt.bar(bar2,y_pred,width,label="y_pred")
plt.xlabel("expected values")
plt.ylabel("predicted values")
plt.title("Barchart for expected vs predicted values")
plt.figure(figsize=(10,10))
plt.show()